<?php

declare(strict_types=1);

namespace Paneric\JWT;

use Exception;

use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Interfaces\Guard\GuardInterface;
use Paneric\Slim\Exception\HttpBadRequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class JWTMiddleware implements MiddlewareInterface
{
    private array $jwtConfig;

    public function __construct(
        ConfigInterface $jwtConfig,
        private readonly GuardInterface $guard
    ) {
        $this->jwtConfig = ($jwtConfig())['decode'];
    }

    /**
     * @throws HttpBadRequestException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $routeName = $request->getAttribute('route_name');

        if (in_array($routeName, $this->jwtConfig['excluded_route_names'], true)) {
            return $handler->handle($request);
        }

        try {
            $authorizationHeader = $request->getHeader('Authorization');

            if (empty($authorizationHeader)) {
                throw new Exception('Missing authorization header');
            }

            $jwt = (str_replace(['Bearer', ' '], '', $authorizationHeader[0]));

            $request = $request->withAttribute(
                $this->jwtConfig['request_attribute_name'],
                ($this->guard->decodeJwtToken($jwt))['sub']
            );
        } catch (Exception $e) {
            throw new HttpBadRequestException($request, 'JWT: ' . $e->getMessage() . ' (error log available).');
        }

        return $handler->handle($request);
    }
}
