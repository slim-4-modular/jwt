<?php

declare(strict_types=1);

namespace Paneric\JWT;

use Paneric\Interfaces\Config\ConfigInterface;

class JWTConfigExample implements ConfigInterface
{
    public function __invoke(): array
    {
        return [
            'encode' => [
                'issuer' => $_ENV['APP_DOMAIN'],
                'expiration' => 12 * 60 * 60,
            ],
            'decode' => [
                'request_attribute_name' => 'jwt',
                'excluded_route_names' => [
                    'auth-api.role-credential.register',

                    'auth-api.credential.request-activation-reset',
                    'auth-api.credential.activate',

                    'auth-api.credential.request-password-reset',
                    'auth-api.credential.reset-password',

                    'auth-api.credential.reset-api-token',

                    'auth-api.credential.log-in',
                ],
            ],
        ];
    }
}
