<?php

declare(strict_types=1);

namespace Paneric\JWT;

use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Interfaces\Guard\GuardInterface;

class JWTEncoder
{
    protected array $jwtConfig;

    public function __construct(
        protected GuardInterface $guard,
        ConfigInterface $jwtConfig,
    ) {
        $this->jwtConfig = ($jwtConfig())['encode'];
    }

    public function __invoke(array $subject, string $audience): string
    {
        $iat = time();
        $payload = [
            'iat' => $iat,
            'iss' => $this->jwtConfig['issuer'],
            'aud' => $audience,
            'sub' => $subject,
            'exp' => $iat + $this->jwtConfig['expiration'],
        ];

        return $this->guard->encodeJwtToken($payload);
    }
}
