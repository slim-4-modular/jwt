<?php

declare(strict_types=1);

namespace Paneric\JWT;

use Paneric\Interfaces\Config\ConfigInterface;
use Paneric\Slim\Exception\HttpUnauthorizedException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class JWTAuthorizationMiddleware implements MiddlewareInterface
{
    private array $jwtConfig;

    public function __construct(
        ConfigInterface $jwtConfig
    ) {
        $this->jwtConfig = ($jwtConfig())['decode'];
    }

    /**
     * @throws HttpUnauthorizedException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $routeName = $request->getAttribute('route_name');

        if (in_array($routeName, $this->jwtConfig['excluded_route_names'], true)) {
            return $handler->handle($request);
        }

        $jwt = $request->getAttribute($this->jwtConfig['request_attribute_name']);

        if (in_array($routeName, $jwt, true)) {
            return $handler->handle($request);
        }

        $routeName = explode('.', $routeName);
        array_pop($routeName);
        $routeName = implode('.', $routeName) . '.*';

        if (in_array($routeName, $jwt, true)) {
            return $handler->handle($request);
        }

        throw new HttpUnauthorizedException(
            $request,
            'JWT: Unauthorized (error log available).'
        );
    }
}
